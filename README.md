# Money saver
Приложение, в котором люди смогут контролировать расходы, ограничить свой бюджет на месяц, определить самые затратные категории

# Участники проекта
- Коростелев Илья Дмитриевич P3223

# Этапы выполнения
1.[HLO+anolugues.md](https://gitlab.com/RedJabka/counting-your-money/-/blob/main/Documentary/HLO+analogues.md)

2.[Прецеденты пользования](https://gitlab.com/RedJabka/counting-your-money/-/blob/main/Documentary/Precedents.md); [Диаграмма прецедентов использования](https://gitlab.com/RedJabka/counting-your-money/-/blob/main/Documentary/Precedents%20Diagram.drawio.png)

3.[Проектирование слоя персистентности](https://gitlab.com/RedJabka/counting-your-money/-/blob/main/Documentary/DataBases.md)

4.[Проектирование интерфейсов](https://gitlab.com/RedJabka/counting-your-money/-/blob/main/Documentary/UI.md)
